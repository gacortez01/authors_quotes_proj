const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose');
const path = require('path');

app.use(bodyParser.json());
//app.use(express.static(path.join(__dirname, '/static'))); 
app.use(express.static(path.join(__dirname, 'quotes-app', 'dist')));

//-----------------------------------------------------------------
mongoose.connect('mongodb://localhost/quote_project');

var AuthorSchema = new mongoose.Schema({
    name: {type: String, minlength: 3},
    quotes: [{
        content: {type: String, minlength:3},
        votes: {type: Number, default: 0}
    }]
})
mongoose.model('Author', AuthorSchema);
var Author = mongoose.model('Author');

app.post('/authors', (req, res)=>{
    var newAuthor = new Author({name: req.body.name});
    newAuthor.save((err)=>{
        if(err){res.json(err)}
        else{res.json(newAuthor)}
    })
})

app.get('/authors', (req, res)=>{
    Author.find({}, (err, foundAuthors)=>{
        if(err){res.json(err)}
        else{res.json(foundAuthors)}
    })
})

app.get('/authors/:id', (req, res)=>{
    Author.findOne({_id: req.params.id}, (err, foundAuthor)=>{
        if(err){res.json(err)}
        else{res.json(foundAuthor)}       
    })
})

app.put('/authors/:id', (req, res)=>{
    Author.findOne({_id: req.params.id}, (err, foundAuthor)=>{
        if(err){res.json(err)}
        else{
            foundAuthor.name = req.body.name;
            foundAuthor.save((err)=>{
                if(err){res.json(err)}
                else{res.json(foundAuthor)}
            })
        }       
    })    
})
app.delete('/authors/:id', (req, res)=>{
    Author.remove({_id: req.params.id}, (err)=>{
        res.json({message: 'author destroyed'})
    })
})
app.post('/quotes/:author_id', (req, res)=>{
    Author.findOne({_id: req.params.author_id}, (err, foundAuthor)=>{
        if(err){res.json(err)}
        else{foundAuthor.quotes.push({content: req.body.content});
            foundAuthor.save((err)=>{
                if(err){res.json(err)}
                else{res.json(foundAuthor)}
            })}
    })
})
//------------------------------------------------------------------
app.all("*", (req,res,next) => {
    res.sendFile(path.resolve("./quotes-app/dist/index.html"))
  });

app.listen(8000, ()=>{
    console.log('listening on port 8000');
});